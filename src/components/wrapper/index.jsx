import { useState, useEffect } from "react";
export default function Wrapper() {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    fetch("https://be-deploy-silk.vercel.app/users")
      .then((res) => res.json())
      .then((data) => setUsers(data));
    //   fetch("http://localhost:8080/posts")
    //     .then((res) => res.json())
    //     .then((data) => {});
  }, []);
  function handleAddUser() {
    const user = {
      id: Date.now(),
      firstName: "Jack",
      lastName: "Black",
      age: 40,
    };
    fetch("https://be-deploy-silk.vercel.app/user", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(user),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUsers((prev) => [...prev, data]);
      });
  }
  return (
    <div>
      {users.length
        ? users.map((user) => (
            <div key={user._id}>{`${user.firstName} ${user.lastName}`}</div>
          ))
        : null}
      <button onClick={handleAddUser}>click me!</button>
    </div>
  );
}
